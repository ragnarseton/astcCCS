 classdef astcCCS < handle
%% astcCCS
% A wrapper class to handle Thorlabs CCS series spectrometers based on the
% TLCCS-library.
%
% About
%	This class was written by Ragnar Seton <ragnar.seton@angstrom.uu.se>, it
%	wraps a bunch of mex-files that in turn basically just wraps the TLCCS API.
%	Note that it is not a one-to-one wrapper since i thought some of the
%	functions were named weirdly (looking at you
%	tlccs_getUserCalibrationPoints/tlccs_setWavelengthData). Also, there's at
%	least one addition (at time of writing this it's just .DataReady) and all
%	the single arg-single return functions have been smushed into a single
%	mex-file.
%
% Requirements
%	Explicit:
%	- TLCCS
%	TODO: add link (check if it's the OSA LabVIEW driver> and add an implicit
%	section with the NI VISA-shenanigans
%
% Notes
%	The two main differences between the TLCCS API and this class are:
%	- tlccs_getUserCalibrationPoints and tlccs_setWavelengthData have been
%	  smushed together into the (dependent) property WavelengthAdjustmentPoints.
%	- tlccs_g/setAmplitudeData with mode = ...NVMEM has been renamed to
%	  PixelAmplitudeAdjustment (for consistency). The function
%	  pixelAmplitudeAdjustScan is used for mode = ...CURRENT/MEAS.
%	See the respective help sections for further info.
%	Also worth noting is that properties with the TLCCS_-prefix are copied from
%	TLCCS.h and if you're using this far in the future (current date:
%	2018-02-09) you should make sure they are correct..
%
% TODO
%	<See the source file and @astcCCS/private/TODO>
%	- Compile the c-files if mex-files are missing
%	- Static gui where you can select the available specs and open their GUIs
%	- GUI for interfacing with the ccs
%	- GUI-callbacks for at least when a spectrum has been fetched
%
% Usage
%	If you only have one CCS device connected it should be safe to simply create
%	an instance of the class without any arguments to the constructor as it (the
%	constructor) will then use the first device it finds. If you have multiple
%	devices connected, or for some reason would like to see the resource name of
%	your single device, use the static function astcCCS.findResources(). This
%	will return the available devices' res names in a cell array, you can then
%	use an item from the array as argument to the constructor, e.g.
%
%	resNames = astcCCS.findResources()
%	% resNames =
%	%	'USB0::0x1313::0x808X::MXXXXXXXX::RAW'
%	%	'USB0::0x1313::0x808X::MXXXXXXXX::RAW'
%	myCCS = astcCCS(resNames{2});
%
%	Before jumping into to the actual usage it's worth mentioning the naming
%	convention of the class; both functions (denoted by lower case first letter)
%	and properties (uppercase first letter) use camelCase with the exception of
%	constant properties (all upper case). Since all but the immutable properties
%	(ResourceName and Session) are dependent, i.e. getting/setting them
%	reads/writes values from/to the device, read-only cached property values
%	(denoted with a C-prefix) are supplied. These can be used in time-sensitive
%	settings such as read loops, e.g.
%
%	% ...setup omitted for brevity
%	evenms = @(s) round(s*1000)/1000;
%	for i = 1 : N
%		writeOnlyStream = myCCS.ScanData; % fetch the last scan data from device
%										  % and update the cache
%		set(plotLineHandle, 'YData', myCCS.CscanData); % use cached data
%		while (~myCCS.DataReady)
%			pause(evenms(myCCS.CintegrationTime/10)); % wait for new data
%		end
%	end
%
%	<more to come...>
	methods(Static = true)
		function r = findResources()
% Returns a cell array with the resource names of all connected CCS devices.
% Each item in the returned array can be given as argument to the astcCCS
% constructor.
%
% See also astcCCS
			r = ccsFindRsrc();
		end
	end
	methods(Static = true, Hidden = true)
% These are the undocumented little nuggets used internally
		function b = toolbarTextButton(tb, uicall, cb, str, tooltip, varargin)
			% ref: https://undocumentedmatlab.com/blog/toolbar-button-labels
			jtb = tb.JavaContainer.getComponentPeer;
			b = uicall(tb, 'ClickedCallback', cb, ...
						   'TooltipString', tooltip, varargin{:});
			drawnow(); pause(0.01); % pause for java component creation
			jb = jtb.getComponent(jtb.getComponentCount() - 1);
			
			%% TODO RETURNERA jb OCH ANVAND DEN I USERDATA!!
			
			gfx = jb.getGraphics();
			w = gfx.getFontMetrics().getStringBounds(str, gfx).getWidth() + 16;
			sz = java.awt.Dimension(w, jb.getHeight());
			jb.setMaximumSize(sz);
			jb.setPreferredSize(sz);
			jb.setSize(sz);
			jb.setText(str);
		end
		function jlbl = toolbarLabel(tb, str, tooltip, varargin)
			jlbl = javaObjectEDT('javax.swing.JLabel', str);
			set(jlbl, 'ToolTipText', tooltip, varargin{:});
			javacomponent(jlbl, [0 0 0 0], tb);
		end
		function success = toolbarSeparator(tb)
    		success = false;
        	jtb = get(get(tb, 'JavaContainer'), 'ComponentPeer');
        	if (isempty(jtb)); return; end
        	jtb(1).addSeparator();
        	end
        	jtb(1).repaint;
        	jtb(1).revalidate;
        	success = true;
		end
	end

	events(NotifyAccess = 'private')
% This one's pretty much replaces DataReady.. but it came later
% Will not be triggered when using an external trigger (use the trigger source 
% and DataReady instead).
%
% See also DataReady
		ScanTransfer;
% Triggered when scanning (without ext. trigger) terminates
		ScanTerminated;
% Triggered if scanning (without ext. trigger) times out
% If this event occurs it is immediately followed by a reset of the device
		ScanTimedOut;
	end

% these should probably be looked up instead of hardcoded somehow..
	properties(Constant = true)
% Number of points in processed scan data.
%
% See also ScanData, WavelengthAdjustmentPoints
		TLCCS_NUM_PIXELS = 3648;
% Number of points in raw scan data.
%
% See also ScanDataRaw
		TLCCS_NUM_RAW_PIXELS = 3694;
% Minimum number of wavelength adjustment points.
%
% See also WavelengthAdjustmentPoints, TLCCS_MAX_NUM_USR_ADJ
		TLCCS_MIN_NUM_USR_ADJ = int32(4);
% Maximum number of wavelength adjustment points.
%
% See also WavelengthAdjustmentPoints, TLCCS_MIN_NUM_USR_ADJ
		TLCCS_MAX_NUM_USR_ADJ = int32(10);
	end

	properties(SetAccess = 'immutable')
% Resource name of the device
		ResourceName;
	end
	
	properties(SetAccess = 'immutable', Hidden = true)
		Session;
	end

	properties(Dependent = true)
% Integration time in seconds.
		IntegrationTime;
% Pixel-amplitude correction factors. The values are stored as a 
% <further info comming>
% Note that this property is persistant on the the device.
%
% See also pixelAmplitudeAdjustScan
		PixelAmplitudeAdjustment;
% Adjustment points should be given as a Nx2 matrix, where
% TLCCS_MIN_NUM_USR_ADJ <= N <= TLCCS_MAX_NUM_USR_ADJ, where each row contains a
% pixel-wavelength pair where pixel values range from 0 to TLCCS_NUM_PIXELS - 1
% (values will be truncated to integers) and wavelengths double values > 0 [nm].
% These pairs will then be used to interpolate the values in the
% WavelengthDataAdjusted vector.
% Note that this property is persistant on the device.
%
% See also TLCCS_MIN_NUM_USR_ADJ, TLCCS_MAX_NUM_USR_ADJ, TLCCS_NUM_PIXELS,
% WavelengthDataAdjusted
		WavelengthAdjustmentPoints;
	end

	properties(Dependent = true, SetAccess = 'private')
% Logical inidicating if new scan data is avaliable (faster way of checking
% Status.ScanTransfer).
% Note that as this value updates frequently and does not affect continuous
% scanning it has no cached value.
%
% See also Status, startScanCont, startScanContExtTrg
		DataReady;
% Identification struct with the following (char valued) fields:
%	ManufacturerName			- Name of the manufacturer.
%	DeviceName					- Name of the device.
%	SerialNumber				- Serial number of the device.
%	FirmwareRevision			- Device firmware revision.
%	InstrumentDriverRevision	- Driver revision.
% Note that the struct is set during construction of the object and should not
% update during in it's lifetime, hence the cached value, Cidentification,
% should be used for reading.
%
% See also Cidentification
		Identification;
% Device status struct with the following (logical valued) fields:
%	ScanIdle		- true if device is waiting for new scan to execute.
%	ScanTriggered	- true if scan is in progress.
%	StartTrans		- true if scan is starting.
%	ScanTransfer	- true if scan is done and data can be read (transfered).
%	WaitForTrigger	- same as ScanIdle except that external trigger is armed.
% Note:
%	- If you're just interested knowing if there's data to fetch you should use
%	  DataReady instead.
%	- As this value updates frequently and does not affect continuous scanning
%	  it has no cached value.
%
% See also DataReady, startScanCont, startScanContExtTrg
		Status;
% Current available processed scan data (if any).
%
% See also ScanDataRaw, DataReady, Status, viewScanData
		ScanData;
% Current available raw scan data (if any).
%
% See also ScanData, DataReady, Status
		ScanDataRaw;
% From WavelengthAdjustmentPoints interpolated wavelength data.
%
% See also WavelengthAdjustmentPoints, WavelengthDataStock
		WavelengthDataAdjusted;
% Wavelength data based on factory adjustment points.
%
% See also WavelengthDataAdjusted
		WavelengthDataStock;
	end
	
	% cached values
	properties(SetAccess = 'private')
% Approximate timespan during which the last ScanData[Raw] was recorded.
% TODO fix the external trigger ones..
%
% See also ScanData, ScanDataRaw
		TimeSpan;
% Cached identification struct.
%
% See also Identification
		Cidentification;
% Cached integration time.
%
% See also IntegrationTime
		CintegrationTime;
% Cached pixel-amplitude adjustment.
%
% See also PixelAmplitudeAdjustment
		CpixelAmplitudeAdjustment;
% Cached scan data.
%
% See also ScanData
		CscanData;
% Cached raw scan data.
%
% See also ScanDataRaw
		CscanDataRaw;
% Cached wavelength adjustment points.
%
% See also WavelengthAdjustmentPoints
		CwavelengthAdjustmentPoints;
% Cached adjusted wavelength data.
%
% See also WavelengthDataAdjusted
		CwavelengthDataAdjusted;
% Cached stock wavelength data.
%
% See also WavelengthDataStock
		CwavelengthDataStock;
	end

% the are just for internal use (mainly gui)
	properties(Access = 'private', Hidden = true)
		% flags set by the buttons in the gui
		guiCont;
		guiExt;
		guiAxesAppend;
		% objects needed by the gui
		guiAxes;
		guiLine;
		guiScanTransferCb;
		guiScanTermCb;
		guiScanTimedOutCb;
		% flag indicating continuous scan state
		contScan;
	end

% these should really be a proper enum, but that's for later..
	properties(Constant = true, Access = 'private', Hidden = true)
		SAF_CLOSE = int32(0);
		SAF_RESET = int32(1);
		SAF_START_SCAN = int32(2);
		SAF_START_SCAN_CONT = int32(3);
		SAF_START_SCAN_CONT_EXT_TRG = int32(4);
		SAF_START_SCAN_EXT_TRG = int32(5);
		TLCCS_CAL_DATA_SET_FACTORY = int16(0);
		TLCCS_CAL_DATA_SET_USER = int16(1);
		% applying stuff to amplitude data
		TLCCS_ACOR_FROM_CURRENT = int32(1);
		TLCCS_ACOR_FROM_NVMEM = int32(2);
		ACOR_GET_FLAG = int32(1073741824); % highest bit (0x40000000) get flag
		% fetching stuff from amplitude data
		TLCCS_ACOR_APPLY_TO_MEAS = int32(1);
		TLLCS_ACOR_APPLY_TO_MEAS_NVMEM = int32(2);
		% status stuff
		TLCCS_STATUS_SCAN_IDLE = uint32(2);
		TLCCS_STATUS_SCAN_TRIGGERED = uint32(4);
		TLCCS_STATUS_SCAN_START_TRANS = uint32(8);
		TLCCS_STATUS_SCAN_TRANSFER = uint32(16);
		TLCCS_STATUS_WAIT_FOR_EXT_TRIG = uint32(128);
	end
	
	% object interface
	methods
		function obj = astcCCS(varargin)
% obj = astcCCS() looks for any connected CCS device and creates obj with the
% first one found (or throws an error if none is found).
%
% obj = astcCCS(rsrc) creates an obj from the given resource name rsrc.
%
% See also findResources
			if (nargin > 0)
				if (isa(varargin{1}, 'char') || isa(varargin{1}, 'string'))
					obj.ResourceName = varargin{1};
				else
					error('astc:ccs:constructor:argin', ...
						  'The supplied argument is not a char or string.');
				end
			else
				nm = ccsFindRsrc();
				if (length(nm) < 1)
					error('astc:ccs:constructor:norsrc', ...
						  'No CCS device found.');
				end
				obj.ResourceName = nm{1};
			end
			obj.Session = ccsInit(obj.ResourceName);
			
			% private stuff
			obj.guiExt = false;
			obj.guiCont = false;
			obj.contScan = false;
			obj.guiAxesAppend = false;
			
			% init the cached values (and timespan)
			% TODO: figure out how and when matlab calls the g/setters to
			% avoid doing it twice
			obj.TimeSpan = [0 0];
			obj.Cidentification = ccsIdentificationQuery(obj.Session);
			obj.CintegrationTime = ccsIntegrationTime(obj.Session);
			obj.CscanData = zeros(1, obj.TLCCS_NUM_PIXELS);
			obj.CscanDataRaw = int32(zeros(1, obj.TLCCS_NUM_RAW_PIXELS));
			obj.CwavelengthDataStock = zeros(1, obj.TLCCS_NUM_PIXELS);
			obj.CwavelengthDataAdjusted = zeros(1, obj.TLCCS_NUM_PIXELS);
			try
				[px, wl] = ccsUserCalibrationPoints(obj.Session);
				obj.CwavelengthAdjustmentPoints = [double(px), wl];
				ccsGetWavelengthData(obj.Session, ...
									 obj.TLCCS_CAL_DATA_SET_USER, ...
									 obj.CwavelengthDataAdjusted);
			catch
				% ignore error to avoid killing constrctor
				obj.CwavelengthAdjustmentPoints = [];
				% CwavelengthDataAdjusted should stay zeros if no calibration
				% points are available
			end
			try
				obj.CpixelAmplitudeAdjustment = ccsAmplitudeData(obj.Session, ...
																 obj.TLCCS_ACOR_FROM_NVMEM);
			catch
				obj.CpixelAmplitudeAdjustment = zeros(1, obj.TLCCS_NUM_PIXELS);
			end
		end
		
		function delete(obj)
% Destructor, should not be called excplicitly
			try
				ccsSingleArgFunc(obj.Session, obj.SAF_CLOSE);
			catch
			end
		end
		
		function close(obj)
% Closes the connection to a CCS device.
% This function should be called when the device is no longer needed. The
% connection can not be restored later but a new object needs to be created.
			obj.contScan = false;
			ccsSingleArgFunc(obj.Session, obj.SAF_CLOSE);
		end
		
		function varargout = pixelAmplitudeAdjustScan(obj, varargin)
% paa = obj.pixelAmplitudeAdjustScan() gets the pixel-amplitude adjustments for
% just the current scan data.
%
% obj.PixelAmplitudeAdjustment(paa) sets the pixel-amplitude adjustments for
% just the current scan data.
%
% See also PixelAmplitudeAdjustment
			if (nargin + nargout > 1); obj.contScan = false; end
			if (nargin > 1)
				ccsAmplitudeData(obj.Session, ...
								 obj.TLLCS_ACOR_APPLY_TO_MEAS, ...
								 varargin{1});
			elseif (nargout > 0)
				varargout{1} = ccsAmplitudeData(obj.Session, ...
												obj.TLCCS_ACOR_FROM_CURRENT);
			end
		end
		
		function reset(obj)
% Resets the device.
			obj.contScan = false;
			ccsSingleArgFunc(obj.Session, obj.SAF_RESET);
		end
		
		function varargout = selfTest(obj)
% NOTE: While exported in the TLCCS header file the function itslef does not
% seem to be implemented, hence the error.
%
% obj.selfTest() performs a self test routine and prints out the result.
% This resets the device.
%
% msg = obj.selfTest() performs a self test and returns the result message.
%
% [msg, resCode] = obj.selfTest() performs a self test and returns both the
% message and the result code.
%
% See also reset

			% commented out as long as it's not implemented in TLCCS.c
%			obj.contScan = false;
			[msg, res] = ccsSelfTest(obj.Session);
			if (nargout < 1)
				disp(['Self test result code: ', num2str(res), newline(), ...
					  'Message: ', msg]);
			else
				varargout{1} = msg;
				if (nargout > 1); varargout{2} = res; end
			end
		end
		
		function startScan(obj)
% Trigger device to take a single scan.
% The scan data will be available when so indicated by DataReady or Status, both
% will update at the same time and while the former is faster to check the
% latter, as the name suggests, contains more information regarding the current
% status of the device.
%
% See also startScanExtTrg, startScanCont, startScanContExtTrg
			obj.contScan = false;
%			disp('single scan');
			ccsSingleArgFunc(obj.Session, obj.SAF_START_SCAN);
			start(timer('StartDelay', round(1000*obj.CintegrationTime/2)/1000, ...
						'TimerFcn', @obj.bgListener));
		end
		function startScanNoEvent(obj)
% Like startScan but without event triggering
%
% See also startScan
			obj.contScan = false;
			ccsSingleArgFunc(obj.Session, obj.SAF_START_SCAN);
		end
		
		function startScanCont(obj)
% Start continuous scanning.
% Calling any function or reading any non-constant (upper case), -immutable
% (ResourceName and Session), or cached (C-prefixed) property other than
% ScanData[Raw], DataReady, or Status will stop the scanning.
%
% See also startScanContExtTrg, startScan, startScanExtTrg, viewScanData
			obj.contScan = true;
			ccsSingleArgFunc(obj.Session, obj.SAF_START_SCAN_CONT);
			start(timer('StartDelay', obj.CintegrationTime/2, ...
						'TimerFcn', @obj.bgListener));
		end
		
		function startScanContExtTrg(obj)
% Arm the device for scanning following the next external trigger (low to high
% transition). The device will rearm immediately after the scan data is read.
% The rearming will be interrupted by the same actions as startScanCont.
%
% See also startScanCont, startScanExtTrg, startScan, viewScanData
			obj.contScan = false;
			ccsSingleArgFunc(obj.Session, obj.SAF_START_SCAN_CONT_EXT_TRG);
		end
		
		function startScanExtTrg(obj)
% Arm the device for single scan following the next external trigger (low to
% high transition).
%
% See also startScan, startScanContExtTrg, startScanCont
			obj.contScan = false;
			ccsSingleArgFunc(obj.Session, obj.SAF_START_SCAN_EXT_TRG);
		end
		
		function viewScanData(obj, nsec, varargin)
% obj.viewScanData(nsec) simply displays ScanData updates in a plot with
% obj.CwavelengthDataStock on the x-axis. Argument nsec is duration in seconds. 
%
% obj.viewScanData(nsec, xvalues) displays the updates with argument xvalues on
% the x-axis, hence xvalues must be an 1xN or Nx1 numeric vector where
% N = obj.TLCCS_NUM_PIXELS.
%
% Note that startScanCont[ExtTrg] must be called before start.
%
% See also ScanData, startScanCont, startScanContExtTrg
			if (nargin > 2)
				if (~isnumeric(varargin{1}) || ...
					length(varargin{1}) ~= obj.TLCCS_NUM_PIXELS)
					error('astcccs:viewscandata:argsz', ...
						  ['xvalues argument must be a 1xN or Nx1 numeric vector where N = ', ...
						   num2str(obj.TLCCS_NUM_PIXELS), '.']);
				end
				xv = varargin{1};
			else
				if obj.CwavelengthDataStock(end) < 1
					ccsGetWavelengthData(obj.Session, ...
										 obj.TLCCS_CAL_DATA_SET_FACTORY, ...
										 obj.CwavelengthDataStock);
				end
				xv = obj.CwavelengthDataStock;
			end
			yticks = (0 : 0.05 : 1);
			f = figure('Renderer', 'opengl');
			ah = axes('Parent', f, ...
					  'TickDir', 'out', ...
					  'XGrid', 'on', ...
					  'XLim', [xv(1) xv(end)], ...
					  'XLimMode', 'manual', ...
					  'YGrid', 'on', ...
					  'YLim', [-0.05, 1.05], ...
					  'YLimMode', 'manual', ...
					  'YTick', yticks, ...
					  'YTickMode', 'manual', ...
					  'YTickLabel', sprintfc('%.2f', yticks), ...
					  'YTickLabelMode', 'manual', ...
					  'ZLimMode', 'manual', ...
					  'ZTickLabelMode', 'manual', ...
					  'ZTickMode', 'manual', ...
					  'WarpToFill', 'on');
			hold(ah, 'on');
			ln = plot(ah, xv, obj.ScanData);
			evenms = @(s) round(s*1000)/1000;
			t0 = tic();
			while (toc(t0) < nsec)
				% TODO: figure out why setting ln.YData in ccsGetScanData
				% doesn't do shit
				if (obj.DataReady)
					ccsGetScanData(obj.Session, false, obj.CscanData);
					set(ln, 'YData', obj.CscanData);
					drawnow();
				else
					pause(evenms(obj.CintegrationTime / 2));
				end
			end
		end
		
		% show the gui
		gui(obj, varargin);

		% getters and setters and such
		function v = get.DataReady(obj)
			v = ccsDataReady(obj.Session);
		end
		
		function v = get.Identification(obj)
			obj.Cidentification = ccsIdentificationQuery(obj.Session);
			v = obj.Cidentification;
		end
		
		function v = get.Status(obj)
			v = ccsGetDeviceStatus(obj.Session);
		end
		
		function v = get.IntegrationTime(obj)
			obj.contScan = false;
			v = ccsIntegrationTime(obj.Session);
			obj.CintegrationTime = v;
		end
		function set.IntegrationTime(obj, v)
			obj.contScan = false;
			ccsIntegrationTime(obj.Session, v);
			obj.CintegrationTime = v;
		end
		
		function v = get.PixelAmplitudeAdjustment(obj)
			obj.contScan = false;
			ccsAmplitudeData(obj.Session, ...
							 int32(obj.ACOR_GET_FLAG + ...
								   obj.TLCCS_ACOR_FROM_NVMEM), ...
							 obj.CpixelAmplitudeAdjustment);
			v = obj.CpixelAmplitudeAdjustment;
		end
		function set.PixelAmplitudeAdjustment(obj, v)
			obj.contScan = false;
			ccsAmplitudeData(obj.Session, ...
							 obj.TLLCS_ACOR_APPLY_TO_MEAS_NVMEM, ...
							 v);
			obj.CpixelAmplitudeAdjustment = v;
		end
		
		function v = get.ScanData(obj)
			ccsGetScanData(obj.Session, false, obj.CscanData);
			v = obj.CscanData;
		end
		function v = get.ScanDataRaw(obj)
			ccsGetScanData(obj.Session, true, obj.CscanDataRaw);
			v = obj.CscanDataRaw;
		end
		
		function v = get.WavelengthAdjustmentPoints(obj)
			obj.contScan = false;
			try
				[px, wl] = ccsUserCalibrationPoints(obj.Session);
				obj.CwavelengthAdjustmentPoints = [double(px), wl];
				v = obj.CwavelengthAdjustmentPoints;
			catch
				% TODO handle this better...
				v = [];
			end
		end
		function set.WavelengthAdjustmentPoints(obj, v)
			% cssUserCalibrationPoints wraps both getUCP and setWLDat,
			% not sure why they were named differently
			obj.contScan = false;
			[r, c] = size(v);
			if (c ~= 2 || ...
				r < obj.TLCCS_MIN_NUM_USR_ADJ || ...
				r > obj.TLCCS_MAX_NUM_USR_ADJ)
				error('astcccs:wavelengthadjustmentpoints:argsz', ...
					  ['Wavelength adjustment points must be between ', ...
					   num2str(obj.TLCCS_MIN_NUM_USR_ADJ), 'x2 and ', ...
					   num2str(obj.TLCCS_MAX_NUM_USR_ADJ), 'x2.']);
			end
			ccsUserCalibrationPoints(obj.Session, int32(v(:,1)), v(:,2));
			obj.CwavelengthAdjustmentPoints = v;
		end
		
		function v = get.WavelengthDataStock(obj)
			obj.contScan = false;
			ccsGetWavelengthData(obj.Session, ...
								 obj.TLCCS_CAL_DATA_SET_FACTORY, ...
								 obj.CwavelengthDataStock);
			v = obj.CwavelengthDataStock;
		end
		function v = get.WavelengthDataAdjusted(obj)
			obj.contScan = false;
			ccsGetWavelengthData(obj.Session, ...
								 obj.TLCCS_CAL_DATA_SET_USER, ...
								 obj.CwavelengthDataAdjusted);
			v = obj.CwavelengthDataAdjusted;
		end
	end
	
	methods(Access = 'private', Hidden = true)
		function bgListener(obj, ~, ~)
%			us = uint32(max(1, round(obj.CintegrationTime / 0.002)));
			if (~obj.contScan)
				obj.TimeSpan(1) = cputime();
				a = obj.waitForScan();
				if (a)
					obj.TimeSpan(2) = cputime();
					obj.notify('ScanTransfer');
				end
% 				state = ccsWaitForScan(obj.Session, us);
% 				if (bitand(obj.TLCCS_STATUS_SCAN_TRANSFER, state) ~= 0)
% 					obj.notify('ScanTransfer');
% 					disp('Got data!');
% 				else
% 					disp('no data for you');
% 				end
			else
				while (obj.contScan)
					obj.TimeSpan(1) = cputime();
					a = obj.waitForScan();
					if (a)
						obj.TimeSpan(2) = cputime();
						obj.notify('ScanTransfer');
					end
				end
%				while (obj.contScan)
%					state = ccsWaitForScan(obj.Session, us);
%					if (bitand(obj.TLCCS_STATUS_SCAN_TRANSFER, state) ~= 0)
%						obj.notify('ScanTransfer');
%					else % if it's not transfer time it's quittin time!
%						break;
%					end
%				end
			end
			obj.notify('ScanTerminated');
		end
		function available = waitForScan(obj)
			available = true;
			waitedTime = 0;
			evenms = @(s) round(s*1000)/1000;
			while (~obj.DataReady)
				pause(evenms(obj.CintegrationTime/2));
				waitedTime = waitedTime + obj.CintegrationTime/2;
				% the five here is not based on anything but testing
				if (waitedTime > 5*obj.CintegrationTime)
					obj.notify('ScanTimedOut');
					obj.reset();
					obj.contScan = false;
					available = false;
					break;
				elseif (waitedTime > 2*obj.CintegrationTime)
					stat = obj.Status();
					if (~stat.ScanTriggered || ...
						~stat.StartTrans)
						% if we've got nothing going we've timed out
						obj.notify('ScanTimedOut');
						obj.reset();
						obj.contScan = false;
						available = false;
						break;
					end
				end
			end
		end
	end
	
	% private (no input validation) utility methods
%	methods(Access = 'protected')
%		function avs = allan(obj)
%			avs = zeros(1, 10);
%		end
%	end
end
