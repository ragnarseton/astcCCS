# astcCCS
A wrapper class to handle Thorlabs CCS series spectrometers based on the
TLCCS-library.

## HOWTO
1. Clone the repo
2. Add a @-prefix to the cloned directory
3. Make sure you have all the requirements installed
3. In MATLAB, navigate to @astcCCS/private, cross your fingers and run `build_all_mex.m`

## About
This class was written by Ragnar Seton <ragnar.seton@angstrom.uu.se>, it
wraps a bunch of mex-files that in turn basically just wraps the TLCCS API.
Note that it is not a one-to-one wrapper since i thought some of the
functions were named weirdly (looking at you
`tlccs_getUserCalibrationPoints`/`tlccs_setWavelengthData`). Also, there's at
least one addition (at time of writing this it's just .DataReady) and all
the single arg-single return functions have been smushed into a single
mex-file.

## Requirements
Explicit:
* TLCCS

TODO: add link (check if it's the OSA LabVIEW driver and add an implicit
section with the NI VISA-shenanigans)

## Notes
The two main differences between the TLCCS API and this class are:
* `tlccs_getUserCalibrationPoints` and `tlccs_setWavelengthData` have been
  smushed together into the (dependent) property `WavelengthAdjustmentPoints`.
* `tlccs_g/setAmplitudeData` with mode = ...NVMEM has been renamed to
  `PixelAmplitudeAdjustment` (for consistency). The function
  `pixelAmplitudeAdjustScan` is used for mode = ...CURRENT/MEAS.
See the respective help sections for further info.
Also worth noting is that properties with the TLCCS\_-prefix are copied from
TLCCS.h and if you're using this far in the future (current date:
2018-02-09) you should make sure they are correct..

## TODO
* See the `astcCCS.m` and `private/TODO`
* Compile the c-files if mex-files are missing
* Static gui where you can select the available specs and open their GUIs
* GUI-callbacks for at least when a spectrum has been fetched

## Usage
If you only have one CCS device connected it should be safe to simply create
an instance of the class without any arguments to the constructor as it (the
constructor) will then use the first device it finds. If you have multiple
devices connected, or for some reason would like to see the resource name of
your single device, use the static function astcCCS.findResources(). This
will return the available devices' res names in a cell array, you can then
use an item from the array as argument to the constructor, e.g.

```matlab
resNames = astcCCS.findResources()
% resNames =
%	'USB0::0x1313::0x808X::MXXXXXXXX::RAW'
%	'USB0::0x1313::0x808X::MXXXXXXXX::RAW'
myCCS = astcCCS(resNames{2});
```

Before jumping into to the actual usage it's worth mentioning the naming
convention of the class; both functions (denoted by lower case first letter)
and properties (uppercase first letter) use camelCase with the exception of
constant properties (all upper case). Since all but the immutable properties
(ResourceName and Session) are dependent, i.e. getting/setting them
reads/writes values from/to the device, read-only cached property values
(denoted with a C-prefix) are supplied. These can be used in time-sensitive
settings such as read loops, e.g.

```matlab
% ...setup omitted for brevity
evenms = @(s) round(s*1000)/1000;
for i = 1 : N
	writeOnlyStream = myCCS.ScanData; % fetch the last scan data from device
					  % and update the cache
	set(plotLineHandle, 'YData', myCCS.CscanData); % use cached data
	while (~myCCS.DataReady)
		pause(evenms(myCCS.CintegrationTime/10)); % wait for new data
	end
end
```

<more to come...>



## License
[BSD-2-Clause](https://opensource.org/license/bsd-2-clause/)
