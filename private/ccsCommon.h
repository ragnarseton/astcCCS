#ifndef _CCS_COMMON_H
#define _CCS_COMMON_H
#ifndef mex_h
	#include "mex.h"
#endif
#ifndef emlrt_h
	#include "emlrt.h"
#endif

#define ERR(id, msg) mexErrMsgIdAndTxt(id, msg)
#ifndef MAX
#	define MAX(x,y)	((x) < (y) ? (y) : (x))
#endif

/* == undocumented libmx-functions == */
/* these work like their documented counterparts but without	*
 * initializing elements and is therefore way faster.			*/
#ifndef EMLRT_VERSION_INFO
#	define UNINIT_NEED_DECL 1
#elif EMLRT_VERSION_INFO < 0x2015a
#	define UNINIT_NEED_DECL 1
#endif
#ifdef UNINIT_NEED_DECL
extern mxArray *mxCreateUninitNumericArray(mwSize, const mwSize*,
										   mxClassID, mxComplexity);
extern mxArray *mxCreateUninitNumericMatrix(mwSize, mwSize, mxClassID,
											mxComplexity);
#endif


#endif /* _CCS_COMMON_H */

