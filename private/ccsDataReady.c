#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID "mexccs:dataready:"

/* args: int32 session	*
 * rets: bool dataReady	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt32 status = 0;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	
	if (nrhs != 1)
		ERR(BASE_ID "nargin", "Exactly one input argument required.");
	if (!mxIsUint32(prhs[0]))
		ERR(BASE_ID "arginclass", "Input argument must be of class uint32.");
	if (nlhs != 1)
		ERR(BASE_ID "nargout", "Exactly one output argument required.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	if ((err = tlccs_getDeviceStatus(instr, &status)) != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(BASE_ID "getdevicestatus", errmsg);
	}
	plhs[0] = mxCreateNumericMatrix(1, 1, mxLOGICAL_CLASS, mxREAL);
	*(bool *)mxGetPr(plhs[0]) = ((status & TLCCS_STATUS_SCAN_TRANSFER) != 0);
}

