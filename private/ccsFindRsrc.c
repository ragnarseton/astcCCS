#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* build with
mex -L'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' -lvisa64 -lTLCCS_64 -I'C:\Program Files\IVI Foundation\VISA\Win64\Include' ccs200Test.c
*/

#define BASE_ID "mexccs:findrsc:"

void exit_with_error(ViSession ses, ViStatus err, const char *eid);

/* args: none							*
 * rets: cellarray of resource strings	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViSession resmgr = VI_NULL;
	ViStatus err	 = VI_SUCCESS;
	ViUInt32 i, n	 = 0;
	ViFindList flist;
	ViChar resstr[VI_FIND_BUFLEN];

	if (nlhs != 1) ERR(BASE_ID "nargout", "Exactly one output argument required.");

//	mexPrintf("Opening default resource manager... ");
	if ((err = viOpenDefaultRM(&resmgr)) != VI_SUCCESS)
		exit_with_error(resmgr, err, BASE_ID "opendefresourcemngr");
//	mexPrintf("Success!\nLooking for resources... ");
	err = viFindRsrc(resmgr, TLCCS_FIND_PATTERN, &flist, &n, resstr);
//	err = viFindRsrc(resmgr, "USB?*", &flist, &n, resstr);
	if (err != VI_SUCCESS && err != VI_ERROR_RSRC_NFOUND)
		exit_with_error(resmgr, err, BASE_ID "findrsrc");

//	mexPrintf("Found %u instrument%s!\n", n, (n != 1 ? "s" : ""));
	plhs[0] = mxCreateCellMatrix((mwSize)n, 1);
	if (n > 0) {
		mxSetCell(plhs[0], 0, mxCreateString(resstr));
		for (i = 1; i < n; i++) {
			if ((err = viFindNext(flist, resstr)) != VI_SUCCESS) {
				viClose(flist);
				exit_with_error(resmgr, err, BASE_ID "findnext");
			}
			mxSetCell(plhs[0], i, mxCreateString(resstr));
		}
	}
	viClose(flist);
	viClose(resmgr);
}

void exit_with_error(ViSession ses, ViStatus err, const char *eid) {
	ViChar buf[TLCCS_ERR_DESCR_BUFFER_SIZE];
	tlccs_error_message(ses, err, buf);
	if (ses != VI_NULL) viClose(ses);
	ERR(eid, buf);
}

// to wrap: init, close, setIntegrationTime, startScan, startScanCont, getScanData, getDeviceStatus

