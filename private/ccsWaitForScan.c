#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

#ifdef _WIN32
#	include <windows.h>
#	define SLEEP(x) Sleep(x)
#else
#	include <unistd.h>
#	define SLEEP(x) usleep(x)
#endif

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID	"mexccs:waitforscan:"
#define EXIT_BM	(TLCCS_STATUS_SCAN_IDLE | TLCCS_STATUS_SCAN_TRANSFER)

/* args: int32 session	*
 * rets: void			*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt32 status = 0;
	ViUInt32 us = 0;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	
	if (nrhs != 2)
		ERR(BASE_ID "nargin", "Exactly two input arguments required.");
	if (!mxIsUint32(prhs[0]) || !mxIsUint32(prhs[1]))
		ERR(BASE_ID "arginclass", "Input arguments must be of class uint32.");
	if (nlhs != 1)
		ERR(BASE_ID "nargout", "Exactly one output argument required.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	us = *(ViSession *)mxGetData(prhs[1]);
	do {
		SLEEP(us);
		if ((err = tlccs_getDeviceStatus(instr, &status)) != VI_SUCCESS) {
			tlccs_error_message(instr, err, errmsg);
			ERR(BASE_ID "getdevicestatus", errmsg);
		}
	} while (!(status & EXIT_BM));
	plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
	*(ViUInt32 *)mxGetPr(plhs[0]) = status;
}

