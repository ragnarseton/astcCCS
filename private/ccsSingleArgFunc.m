% wrapper to call funcs that only take an inited instrument as arg and has no
% return value (other than error).
%
% enum fns {
% 	FN_NOFUNC = -1,
% 	FN_CLOSE,
% 	FN_RESET,
% 	FN_START_SCAN,
% 	FN_START_SCAN_CONT,
% 	FN_START_SCAN_CONT_EXT_TRG,
% 	FN_START_SCAN_EXT_TRG
% };
