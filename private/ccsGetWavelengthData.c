#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID "mexccs:getwavelengthdata:"

/* args: int32 session, enum dataset [, double *buf]				* 
 * rets: if buf not set then the wavelength data, otherwise nothing	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt16 set;
	size_t m, n;
	mxArray *dat;
	const char *errid;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	
	if (nrhs != 2 && nrhs != 3)
		ERR(BASE_ID "nargin",
			"Exactly two or three input arguments required.");
	if (nrhs == 2 && nlhs != 1)
		ERR(BASE_ID "nargout",
			"Exactly one output argument required for two input arguments.");
	if (nrhs == 3 && nlhs != 0)
		ERR(BASE_ID "nargout",
			"No output arguments are supported for three input arguments.");
	if (!mxIsUint32(prhs[0]) || !mxIsInt16(prhs[1]))
		ERR(BASE_ID "arginclass01",
			"Arguments must be of classes uint32 and int16.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	set = *(ViInt16 *)mxGetData(prhs[1]);
	if (set != TLCCS_CAL_DATA_SET_FACTORY && set != TLCCS_CAL_DATA_SET_USER)
		ERR(BASE_ID "dataset", "Invalid dataset selection.");

	if (nrhs == 3) {
		if (!mxIsDouble(prhs[2]))
			ERR(BASE_ID "arginclass2",
				"Third input argument for wavelength data must be of class double.");
		m = mxGetM(prhs[2]);
		n = mxGetN(prhs[2]);
		if ((MAX(m, n) < TLCCS_NUM_PIXELS) || (m != 1 && n != 1))
			ERR(BASE_ID "arginsize",
				"Third argument must be of size 1xN or Nx1 where N >= TLCCS_NUM_PIXELS");
		dat = (mxArray *)prhs[2];
	} else {
		dat = mxCreateUninitNumericMatrix(1, TLCCS_NUM_PIXELS, mxDOUBLE_CLASS, mxREAL);
	}

	err = tlccs_getWavelengthData(instr, set, (ViReal64 *)mxGetPr(dat), NULL, NULL);
	if (err != VI_SUCCESS) {
		if (nrhs == 2) mxDestroyArray((mxArray *)dat);
		tlccs_error_message(instr, err, errmsg);
		ERR(BASE_ID "call", errmsg);
	}
	if (nrhs == 2) plhs[0] = dat;
}

