#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

/* note lack of : at the end! */
#define BASE_ID "mexccs:identificationquery"
#define NO_FIELDS 5

/* args: int32 session						*
 * rets: struct with identification data	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	const char *flds[] = {
		"ManufacturerName",	"DeviceName",
		"SerialNumber",		"FirmwareRevision",
		"InstrumentDriverRevision"
	};
	ViChar manNm[256], devNm[256], sn[256], fwRev[256], idRev[256];
	unsigned int i;
	
	if (nrhs != 1)
		ERR(BASE_ID "nargin", "Exactly one input argument is required.");
	if (nlhs != 1)
		ERR(BASE_ID "nargout", "Exactly one output argument is required.");
	if (!mxIsUint32(prhs[0]))
		ERR(BASE_ID "arginclass", "Input argument must be of classe uint32.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	err = tlccs_identificationQuery(instr, manNm, devNm, sn, fwRev, idRev);
	if (err != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(BASE_ID, errmsg);
	}
	plhs[0] = mxCreateStructMatrix(1, 1, NO_FIELDS, flds);
	mxSetFieldByNumber(plhs[0], 0, 0, mxCreateString(manNm));
	mxSetFieldByNumber(plhs[0], 0, 1, mxCreateString(devNm));
	mxSetFieldByNumber(plhs[0], 0, 2, mxCreateString(sn));
	mxSetFieldByNumber(plhs[0], 0, 3, mxCreateString(fwRev));
	mxSetFieldByNumber(plhs[0], 0, 4, mxCreateString(idRev));
}

