#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* NOTE: this file also wraps setWavelengthData */

#define BASE_ID "mexccs:usercalibrationpoints:"

/* args: int32 session [, int32 *pxs, double *wavelns]		*
 * rets: if 1 in, int32 *pxs and double *wavelns else none	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt32 pxbuf[TLCCS_MAX_NUM_USR_ADJ], *pxdat;
	ViReal64 wlbuf[TLCCS_MAX_NUM_USR_ADJ], *wldat;
	ViInt32 i, buflen;
	size_t pxr, wlr;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	const char *errid;
	
	if (nrhs != 1 && nrhs != 3)
		ERR(BASE_ID "nargin",
			"Exactly one or three input argument required.");
	if (!mxIsUint32(prhs[0]))
		ERR(BASE_ID "arginclass",
			"First input argument must be of class uint32.");
	if (nrhs == 1 && nlhs != 2)
		ERR(BASE_ID "nargout",
			"With one input argument two output arguments are required.");
	if (nrhs == 3 && nlhs != 0)
		ERR(BASE_ID "nargout",
			"With three input arguments no output arguments are supported.");
	if (nrhs == 3 && (!mxIsInt32(prhs[1]) || !mxIsDouble(prhs[2])))
		ERR(BASE_ID "arginclass",
			"Second and third input arguments must be of class int32 and double.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	if (nrhs == 1) {
		err = tlccs_getUserCalibrationPoints(instr, pxbuf, wlbuf, &buflen);
		errid = BASE_ID "get";
	} else {
		pxr = mxGetM(prhs[1]);
		wlr = mxGetM(prhs[2]);
		if (pxr != wlr)
			ERR(BASE_ID "arginlengths",
				"Second and third argument must be of the same length.");
		err = tlccs_setWavelengthData(instr,
									  (ViInt32 *)mxGetPr(prhs[1]),
									  (ViReal64 *)mxGetPr(prhs[2]),
									  pxr);
		errid = BASE_ID "set";
	}
	if (err != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(errid, errmsg);
	}
	if (nlhs > 0) {
		plhs[0] = mxCreateUninitNumericMatrix(buflen, 1, mxINT32_CLASS, mxREAL);
		pxdat = (ViInt32 *)mxGetPr(plhs[0]);
		plhs[1] = mxCreateUninitNumericMatrix(buflen, 1, mxDOUBLE_CLASS, mxREAL);
		wldat = (ViReal64 *)mxGetPr(plhs[1]);
		for (i = 0; i < buflen; i++) {
			pxdat[i] = pxbuf[i];
			wldat[i] = wlbuf[i];
		}
	}
}

