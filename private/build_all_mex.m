% TODO
% - check 32/64-bit platforms (computer())
% - try to locate VISA and TLCCS in some nifty way
if (ismac())
	disp(' = Building for mac =');
	error('Sorry, can''t do that yet!');
elseif (isunix())
	disp(' = Building for linux =');
	disp('(this is still HIGHLY experimental! see README for TLCCS info)');
	libPaths = { '/usr/local/lib64', '/usr/local/vxipnp/linux/lib64' };
	libs = { 'visa', 'm' }; % last one needed for TLCCS lib
	incPaths = { '/usr/local/include' };
	fns = {	'ccsAmplitudeData.c TLCCS.c', ...
			'ccsDataReady.c TLCCS.c', ...
			'ccsFindRsrc.c TLCCS.c', ...
			'ccsGetDeviceStatus.c TLCCS.c', ...
			'ccsGetScanData.c TLCCS.c', ...
			'ccsGetWavelengthData.c TLCCS.c', ...
			'ccsIdentificationQuery.c TLCCS.c', ...
			'ccsInit.c TLCCS.c', ...
			'ccsIntegrationTime.c TLCCS.c', ...
			'ccsSelfTest.c TLCCS.c', ...
			'ccsSingleArgFunc.c TLCCS.c', ...
			'ccsUserCalibrationPoints.c TLCCS.c', ...
			'ccsWaitForScan.c TLCCS.c' };
elseif (ispc())
	disp(' = Building for windows =');
	disp('(hope you have VISA and TLCCS installed in the default dirs and you''re on a x86_64 machine)');
	% these should be updated based on bityness
	libPaths = { 'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' };
	libs = { 'visa64', 'TLCCS_64' };
	incPaths = { 'C:\Program Files\IVI Foundation\VISA\Win64\Include' };
	fns = {	'ccsAmplitudeData.c', ...
			'ccsDataReady.c', ...
			'ccsFindRsrc.c', ...
			'ccsGetDeviceStatus.c', ...
			'ccsGetScanData.c', ...
			'ccsGetWavelengthData.c', ...
			'ccsIdentificationQuery.c', ...
			'ccsInit.c', ...
			'ccsIntegrationTime.c', ...
			'ccsSelfTest.c', ...
			'ccsSingleArgFunc.c', ...
			'ccsUserCalibrationPoints.c', ...
			'ccsWaitForScan.c' };
end
mexcall = [ 'mex -L''',	strjoin(libPaths, ''' -L'''), ...
			''' -l',	strjoin(libs, ' -l'), ...
			' -I''',	strjoin(incPaths, ''' -I'''), ...
			''' ' ];
for i = 1 : length(fns)
	disp(['Building ', fns{i}, '...']);
	eval([mexcall, fns{i}])
end

