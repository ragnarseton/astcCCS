#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* build with
mex -L'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' -lvisa64 -lTLCCS_64 -I'C:\Program Files\IVI Foundation\VISA\Win64\Include' ccs200Test.c
*/

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID "mexccs:getscandata:"

/* args: int32 session, bool raw [, double *buf]				*
 * rets: if buf not set then the scan data, otherwise nothing	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	mxLogical raw = 0;
	size_t m, n, nopx = TLCCS_NUM_PIXELS;
	mxClassID cid = mxDOUBLE_CLASS;
	mxArray *dat;
	const char *errid;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	
	if (nrhs != 2 && nrhs != 3)
		ERR(BASE_ID "nargin",
			"Exactly two or three input arguments required.");
	if (nrhs == 2 && nlhs != 1)
		ERR(BASE_ID "nargout",
			"Exactly one output argument required for two input arguments.");
	if (nrhs == 3 && nlhs != 0)
		ERR(BASE_ID "nargout",
			"No output arguments are supported for three input arguments.");
	if (!mxIsUint32(prhs[0]) || !mxIsLogical(prhs[1]))
		ERR(BASE_ID "arginclass01",
			"Arguments must be of classes uint32 and logical.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	if (raw = *(mxLogical *)mxGetData(prhs[1])) {
		nopx = TLCCS_NUM_RAW_PIXELS;
		cid = mxINT32_CLASS;
	}

	if (nrhs == 3) {
		m = mxGetM(prhs[2]);
		n = mxGetN(prhs[2]);
		if (MAX(m, n) < nopx || (m != 1 && n != 1))
			ERR(BASE_ID "arginsize",
				"Third argument must be of size 1xN or Nx1 where N >= TLCCS_NUM_PIXELS");
		if (raw && !mxIsInt32(prhs[2]))
			ERR(BASE_ID "arginclass2",
				"Third input argument for raw scan data must be of class int32.");
		if (!raw && !mxIsDouble(prhs[2]))
			ERR(BASE_ID "arginclass2",
				"Third input argument for scan data must be of class double.");
		dat = (mxArray *)prhs[2];
		// TODO mark it as updated in some way..
	} else {
		dat = mxCreateUninitNumericMatrix(1, nopx, cid, mxREAL);
	}
	
	if (raw) {
		err = tlccs_getRawScanData(instr, (ViInt32 *)mxGetPr(dat));
		errid = BASE_ID "raw";
	} else {
		err = tlccs_getScanData(instr, (ViReal64 *)mxGetPr(dat));
		errid = BASE_ID "processed";
	}
	if (err != VI_SUCCESS) {
		if (nrhs == 2) mxDestroyArray((mxArray *)dat);
		tlccs_error_message(instr, err, errmsg);
		ERR(errid, errmsg);
	}
	if (nrhs == 2) plhs[0] = dat;
}

