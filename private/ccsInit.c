#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* build with
mex -L'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' -lvisa64 -lTLCCS_64 -I'C:\Program Files\IVI Foundation\VISA\Win64\Include' ccs200Test.c
*/

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID "mexccs:init:"

/* args: char *resourceName	*
 * rets: int32 session		*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	char *resstr;
	ViSession instr = VI_NULL;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	
	if (nrhs != 1)
		ERR(BASE_ID "nargin", "Exactly one input argument required.");
	else if(!mxIsChar(prhs[0]))
		ERR(BASE_ID "arginclass", "Input argument must be of class char.");
	if (nlhs != 1)
		ERR(BASE_ID "nargout", "Exactly one output argument required.");
	resstr = mxArrayToString(prhs[0]);
	/* don't send indentification query or reset device during init process */
	if ((err = tlccs_init(resstr, VI_OFF, VI_OFF, &instr)) != VI_SUCCESS) {
//		mexPrintf("status code: %08x\n", err);
		tlccs_error_message(instr, err, errmsg);
		if (instr != VI_NULL) viClose(instr);
		ERR(BASE_ID "init", errmsg);
	}
	plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
	*(ViUInt32 *)mxGetPr(plhs[0]) = instr;
}

