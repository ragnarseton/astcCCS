#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

#define BASE_ID "mexccs:selftest:"

/* args: int32 session					*
 * rets: char *resultMsg, int16 result	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt16 res;
	ViChar msg[256], errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	
	if (nrhs != 1)
		ERR(BASE_ID "nargin", "Exactly one input argument required.");
	if (!mxIsUint32(prhs[0]))
		ERR(BASE_ID "arginclass", "Input argument must be of class uint32.");
	if (nlhs != 2)
		ERR(BASE_ID "nargout", "Exactly two output arguments required.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	if ((err = tlccs_self_test(instr, &res, msg)) != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(BASE_ID "call", errmsg);
	}
	plhs[0] = mxCreateString(msg);
	plhs[1] = mxCreateNumericMatrix(1, 1, mxINT16_CLASS, mxREAL);
	*(ViInt16 *)mxGetPr(plhs[1]) = res;
}

