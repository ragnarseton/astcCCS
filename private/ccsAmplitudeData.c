#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

#define BASE_ID "mexccs:amplitudedata:"

/* args: int32 session, int32 mode [, double *ampData || , double *adBuf]	*
 * rets: if 1 arg double *ampData, else none								*/
/* NOTE: mode can carry a get-flag in the highest bit (30, not sign bit) which
 * needs to be set if the supplied buffer should be used to store the result.
 */
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt32 mode;
	mxArray *dat;
	size_t m, n;
	bool get = true;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	const char *errid;
	
	if (nrhs != 2 && nrhs != 3)
		ERR(BASE_ID "nargin",
			"Exactly two or three input arguments are required.");
	if (!mxIsUint32(prhs[0]) || !mxIsInt32(prhs[1]))
		ERR(BASE_ID "arginclass",
			"First and second input arguments must be of class uint32 and int32.");
	if (nrhs == 2 && nlhs != 1)
		ERR(BASE_ID "nargout",
			"With two input arguments one output argument is required.");
	if (nrhs == 3) {
		if (nlhs != 0)
			ERR(BASE_ID "nargout",
				"With three input arguments output arguments are not supported.");
		if (!mxIsDouble(prhs[2]))
			ERR(BASE_ID "arginclass",
				"Third input argument must be of class double.");
		m = mxGetM(prhs[2]);
		n = mxGetN(prhs[2]);
		if (MAX(m, n) < TLCCS_NUM_PIXELS || (m != 1 && n != 1))
			ERR(BASE_ID "arginsz",
				"Third argument must be a 1xN or Nx1 matrix where N >= TLCCS_NUM_PIXELS.");
	}
	
	instr = *(ViSession *)mxGetData(prhs[0]);
	mode = *(ViInt32 *)mxGetData(prhs[1]);
	if (((mode >> 30) & 0x01) != 0) { /* get flag with buf */
		if (nrhs != 3)
			ERR(BASE_ID "getflag:nargin",
				"With the get flag set a recieving buffer is required as third argument.");
		dat = (mxArray *)prhs[2];
		mode &= 0xbfffffff;
	} else if (nrhs == 2) {
		dat = mxCreateUninitNumericMatrix(1, TLCCS_NUM_PIXELS,
										  mxDOUBLE_CLASS, mxREAL);
	} else {
		get = false;
	}

	if (get) {
		err = tlccs_getAmplitudeData(instr,
									 (ViReal64 *)mxGetPr(dat),
									 0, TLCCS_NUM_PIXELS,
									 mode);
		errid = BASE_ID "get";
	} else {
		err = tlccs_setAmplitudeData(instr,
									 (ViReal64 *)mxGetPr(prhs[2]),
									 TLCCS_NUM_PIXELS, 0,
									 mode);
		errid = BASE_ID "set";
	}
	if (err != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(errid, errmsg);
	}
	if (nrhs == 2) plhs[0] = dat;
}

