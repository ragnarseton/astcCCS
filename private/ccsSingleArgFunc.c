#include "mex.h"
#include <visa.h>
#include <TLCCS.h>

/* build with
mex -L'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' -lvisa64 -lTLCCS_64 -I'C:\Program Files\IVI Foundation\VISA\Win64\Include' ccs200Test.c
*/

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID "mexccs:singlearg:"
#define ERR(id, msg) mexErrMsgIdAndTxt(id, msg)

enum fns {
	FN_NOFUNC = -1,
	FN_CLOSE,
	FN_RESET,
	FN_START_SCAN,
	FN_START_SCAN_CONT,
	FN_START_SCAN_CONT_EXT_TRG,
	FN_START_SCAN_EXT_TRG
};

/* args: int32 session, enum fns	*
 * rets: none						*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	enum fns fn = FN_NOFUNC;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	const char *errid;
	
	if (nrhs != 2)
		ERR(BASE_ID "nargin", "Exactly two input arguments are required.");
	if (nlhs != 0)
		ERR(BASE_ID "nargout", "No output argument is supported.");
	if (!mxIsUint32(prhs[0]) || !mxIsInt32(prhs[1]))
		ERR(BASE_ID "arginclass",
			"Arguments must be of classes uint32 and int32.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	fn = *(enum fns *)mxGetData(prhs[1]);
	switch (fn) {
		case FN_CLOSE:
			err = tlccs_close(instr);
			errid = BASE_ID "close";
			break;
		case FN_RESET:
			err = tlccs_reset(instr);
			errid = BASE_ID "reset";
			break;
		case FN_START_SCAN:
			err = tlccs_startScan(instr);
			errid = BASE_ID "startscan";
			break;
		case FN_START_SCAN_CONT:
			err = tlccs_startScanCont(instr);
			errid = BASE_ID "startscancont";
			break;
		case FN_START_SCAN_CONT_EXT_TRG:
			err = tlccs_startScanContExtTrg(instr);
			errid = BASE_ID "startscancontexttrg";
			break;
		case FN_START_SCAN_EXT_TRG:
			err = tlccs_startScanExtTrg(instr);
			errid = BASE_ID "startscanexttrg";
			break;
		default:
			ERR(BASE_ID, "Unknown function.");
	}
	if (err != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(errid, errmsg);
	}
}

