#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* build with
mex -L'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' -lvisa64 -lTLCCS_64 -I'C:\Program Files\IVI Foundation\VISA\Win64\Include' ccs200Test.c
*/

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

/* note lack of : at the end! */
#define BASE_ID "mexccs:getdevicestatus"
#define NO_FIELDS 5

/* args: int32 session				*
 * rets: struct with status bits	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViInt32 status = 0;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	const char *flds[] = {
		"ScanIdle",		"ScanTriggered",
		"StartTrans",	"ScanTransfer",
		"WaitForTrigger"
	};
	const ViInt32 masks[] = {
		TLCCS_STATUS_SCAN_IDLE,			TLCCS_STATUS_SCAN_TRIGGERED,
		TLCCS_STATUS_SCAN_START_TRANS,	TLCCS_STATUS_SCAN_TRANSFER,
		TLCCS_STATUS_WAIT_FOR_EXT_TRIG
	};
	unsigned int i;
	
	if (nrhs != 1)
		ERR(BASE_ID "nargin", "Exactly one input argument is required.");
	if (nlhs != 1)
		ERR(BASE_ID "nargout", "Exactly one output argument is required.");
	if (!mxIsUint32(prhs[0]))
		ERR(BASE_ID "arginclass", "Input argument must be of classe uint32.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	if ((err = tlccs_getDeviceStatus(instr, &status)) != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(BASE_ID, errmsg);
	}
	plhs[0] = mxCreateStructMatrix(1, 1, NO_FIELDS, flds);
	for (i = 0; i < NO_FIELDS; i++)
		mxSetFieldByNumber(plhs[0], 0, i,
						   mxCreateLogicalScalar((bool)(status & masks[i])));
}

