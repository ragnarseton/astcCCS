#include "mex.h"
#include "ccsCommon.h"
#include <visa.h>
#include <TLCCS.h>

/* build with
mex -L'C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc' -lvisa64 -lTLCCS_64 -I'C:\Program Files\IVI Foundation\VISA\Win64\Include' ccs200Test.c
*/

/* Check visa.h and visatype.h typdefing ViSession to ViObject, ViObject to
 * ViUInt32, and ViUInt32 to a 32 bit wide unsigned integer.
 * ViChar is also typedefed to char in visatype.h
 */

#define BASE_ID "mexccs:integrationtime:"

/* args: int32 session [, double intTime]			*
 * rets: if intTime is set nothing, else intTime	*/
void mexFunction(int nlhs, mxArray **plhs,
				 int nrhs, const mxArray **prhs) {
	ViStatus err = VI_SUCCESS;
	ViSession instr = VI_NULL;
	ViReal64 inttime;
	ViChar errmsg[TLCCS_ERR_DESCR_BUFFER_SIZE];
	const char *errid;
	
	if (nrhs != 2 && nrhs != 1)
		ERR(BASE_ID "nargin", "One or two input argument required.");
	if (!mxIsUint32(prhs[0]))
		ERR(BASE_ID "arginclass", "First input argument must be of class uint32.");
	if (nrhs == 2 && !mxIsDouble(prhs[1]))
		ERR(BASE_ID "arginclass", "Second input argument must be of class double.");

	instr = *(ViSession *)mxGetData(prhs[0]);
	if (nrhs == 2) {
		inttime = *(ViReal64 *)mxGetData(prhs[1]);
		err = tlccs_setIntegrationTime(instr, inttime);
		errid = BASE_ID "set";
	} else {
		err = tlccs_getIntegrationTime(instr, &inttime);
		errid = BASE_ID "get";
	}
	if (err != VI_SUCCESS) {
		tlccs_error_message(instr, err, errmsg);
		ERR(errid, errmsg);
	}
	if (nlhs > 0) {
		plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
		*(ViReal64 *)mxGetPr(plhs[0]) = inttime;
	} else if (nrhs == 1) {
		mexPrintf("Integration Time [s] = %f\n", inttime);
	}
}

